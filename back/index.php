<?php

require_once __DIR__ . '/vendor/autoload.php';

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

$app = new App();

$container = $app->getContainer();
$container['upload_directory'] = __DIR__ . '/uploads';

// This middleware will append the response header Access-Control-Allow-Methods with all allowed methods
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

//----------------------------------------------------------------------------------------------------------------------

$app->post('/', function (Request $request, Response $response) {
    $directory = $this->get('upload_directory');

    $uploadedFiles = $request->getUploadedFiles();

    foreach ($uploadedFiles as &$file) {

        // handle single input with single file upload

        if ($file == null) {
            return $response->withStatus(400, 'Error post file');
        }

        $ext = pathinfo($file->getClientFilename(), PATHINFO_EXTENSION);
        if ($ext != "pdf") {
            return $response->withStatus(400, "Wrong extension, only pdf");
        }

        if ($file->getError() === UPLOAD_ERR_OK) {
            $filename = moveUploadedFile($directory, $file);
            $response->getBody()->write($filename);
        }
    }
    return $response;
});

$app->get("/files", function (Request $request, Response $response) {
    $directory = $this->get('upload_directory');
    $list = getAllFiles($directory);
    return $response->getBody()->write(json_encode($list));
});

$app->post("/file/{filename}", function (Request $request, Response $response, array $args) {
    $filename = $args["filename"];
    $filename .= ".pdf";

    $directory = $this->get('upload_directory');
    $filepath = $directory . '/' . $filename;

    $response = $response->withHeader('Content-Description', 'File Transfer')
        ->withHeader('Content-Type', 'application/octet-stream')
        ->withHeader('Content-Disposition', 'attachment;filename="' . $filename . '"')
        ->withHeader('Expires', '0')
        ->withHeader('Cache-Control', 'must-revalidate')
        ->withHeader('Pragma', 'public')
        ->withHeader('Content-Length', filesize($filepath));
    readfile($filepath);
    return $response;
});

$app->delete("/file/{filename}", function (Request $request, Response $response, array $args) {
    $filename = $args["filename"];
    $filename .= ".pdf";
    $directory = $this->get('upload_directory');
    $filepath = $directory . '/' . $filename;

    $success = unlink($filepath);
    if (!$success) {
        return $response->withStatus(404, "File not found");
    }
    return $response->withStatus(201, "Deleted");
});

$app->run();

//----------------------------------------------------------------------------------------------------------------------

/**
 * Moves the uploaded file to the upload directory and assigns it a unique name
 * to avoid overwriting an existing uploaded file.
 *
 * @param string $directory directory to which the file is moved
 * @param UploadedFile $uploadedFile file uploaded file to move
 * @return string filename of moved file
 * @throws Exception
 */
function moveUploadedFile(string $directory, UploadedFile $uploadedFile): string
{
    $filename = $uploadedFile->getClientFilename();
    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

    return $filename;
}

/**
 * List all files in upload directory
 *
 * @param string $directory directory including files
 * @return array
 */
function getAllFiles(string $directory): array
{
    $fileList = glob($directory . '/*');
    $list = [];
    foreach ($fileList as $filename) {
        //Use the is_file function to make sure that it is not a directory.
        if (is_file($filename)) {
            $fileStats = getStatFile($filename);
            array_push($list, $fileStats);
        }
    }
    return $list;
}

/**
 * Get stats for filename
 *
 * @param string $filename
 * @return File
 */
function getStatFile(string $filename): File
{
    $fileStats = new File();
    $fileStats->ctime = filectime($filename);
    $fileStats->size = fileSizeFormatted($filename);
    $fileStats->name = basename($filename);

    return $fileStats;
}

/**
 * Get file size
 *
 * @param $path
 * @return string
 */
function fileSizeFormatted($path): string
{
    $size = filesize($path);
    $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    $power = $size > 0 ? floor(log($size, 1024)) : 0;
    return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
}

class File
{
    public $name;
    public $ctime;
    public $size;
}