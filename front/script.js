let url = 'http://localhost:8888/'

let error = document.getElementById('error')
let wrong = document.getElementById('wrong')

attachFormSubmitEvent('form')

reloadTable()

function dateFormat(date) {
    return date.toISOString().slice(0, 10)
}

function attachFormSubmitEvent(formID) {
    document.getElementById(formID).addEventListener('submit', formSubmit)
}

/**
 * Reload content of table
 * @param event
 */
function reloadTable(event) {
    error.style.display = 'none'
    wrong.style.display = 'none'

    let list = document.getElementById('list')
    if (list.childElementCount > 1)
        list.removeChild(list.children[list.childElementCount - 1])
    axios.get(url + 'files').then(res => {
        if (res !== undefined && res.data !== undefined && res.data !== '') {
            let node = document.createElement('tbody')
            let lines = ''
            for (let i = 0; i < res.data.length; i++) {
                let obj = res.data[i]
                let cDate = new Date(obj.ctime * 1000)
                let nameWithoutEx = obj.name.split('.').shift()
                lines += '<tr>' +
                    '<td>' + obj.name + '</td>' +
                    '<td>' + obj.size + '</td>' +
                    '<td>' + dateFormat(cDate) + '</td>' +
                    '<td class="download-x" onclick="previewFile(`' + nameWithoutEx + '`)"><i class="fas fa-download"></i></i></td>' +
                    '<td class="delete-x" onclick="deleteFile(`' + nameWithoutEx + '`)"><i class="fas fa-trash-alt"></i></td>' +
                    '</tr>'
            }
            node.innerHTML = lines
            list.append(node)
        }
    }).catch(err => {
        console.log(err)
        error.style.display = 'block'
    })
    if (event !== undefined)
        event.preventDefault()
}

/**
 * Preview a file using his name
 *
 * @param filename
 */
function previewFile(filename) {
    axios({
        url: url + 'file/' + filename, //your url
        method: 'POST',
        responseType: 'blob', // important
    }).then((response) => {
        console.log(response)
        const url = window.URL.createObjectURL(new Blob([response.data]))
        const link = document.createElement('a')
        link.href = url
        link.setAttribute('download', filename + '.pdf') //or any other extension
        document.body.appendChild(link)
        link.click()
    }).catch(err => {
        console.log(err)
        error.style.display = 'block'
    })
}

/**
 * Delete a file using his name
 *
 * @param filename
 */
function deleteFile(filename) {
    axios.delete(url + 'file/' + filename).then(res => {
        console.log(res)
        reloadTable(event)
    }).catch(err => {
        console.log(err)
        error.style.display = 'block'
    })
}

/**
 * Handler for form submit
 *
 * @param event
 */
function formSubmit(event) {
    let input = document.getElementById('file')
    let file = input.files[0]

    if (file !== undefined) {
        error.style.display = 'none'
        wrong.style.display = 'none'
        const formData = new FormData()
        formData.append(file.name, file)

        // Post the form, just make sure to set the 'Content-Type' header
        axios.post(url, formData).then(res => {
            reloadTable(event)
        }).catch(err => {
            console.log(err)
            console.log()
            if (err.response.status === 400) {
                wrong.style.display = 'block'
            } else {
                error.style.display = 'block'
            }
        })
    } else {
        error.style.display = 'block'
    }

    event.preventDefault()
}

/**
 * Sort table using index of column
 * @param n
 */
function sortTable(n) {
    let table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0
    table = document.getElementById('list')
    switching = true
    // Set the sorting direction to ascending:
    dir = 'asc'
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false
        rows = table.rows
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName('TD')[n]
            y = rows[i + 1].getElementsByTagName('TD')[n]
            /* Check if the two rows should switch place,
            based on the direction, asc or desc: */
            if (dir === 'asc') {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true
                    break
                }
            } else if (dir === 'desc') {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true
                    break
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i])
            switching = true
            // Each time a switch is done, increase this count by 1:
            switchcount++
        } else {
            /* If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again. */
            if (switchcount === 0 && dir === 'asc') {
                dir = 'desc'
                switching = true
            }
        }
    }
}
