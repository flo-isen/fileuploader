# Main makefile

view:
	@cd front/ && make

server:
	@cd back/ && make

install:
	@cd back/ && make install
	@cd front/ && make install
